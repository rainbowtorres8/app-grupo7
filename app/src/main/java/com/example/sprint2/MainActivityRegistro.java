package com.example.sprint2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivityRegistro extends AppCompatActivity {

    private TextView nom;
    private TextView usu;
    private TextView pass;
    private TextView pass1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_registro);

        nom=(EditText)findViewById(R.id.edit1);
        usu=(EditText)findViewById(R.id.edit2);
        pass=(EditText)findViewById(R.id.Password1);
        pass1 =(EditText)findViewById(R.id.password2);
    }

    //Boton para regresar al inicio de sesión
    public void volver(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion",null,1);
        SQLiteDatabase db=admin.getWritableDatabase();

        String nombre_usu=nom.getText().toString();
        String usuario=usu.getText().toString();
        String password=pass.getText().toString();
        String password1=pass1.getText().toString();

        if (nombre_usu.length()==0){
            Toast.makeText(MainActivityRegistro.this, "Debes ingresar Nombre y Apellidos", Toast.LENGTH_SHORT).show();
        }else if (usuario.length()==0){
            Toast.makeText(MainActivityRegistro.this, "Debes ingresar un nombre de Usuario", Toast.LENGTH_SHORT).show();
        }else if (password.length()==0){
            Toast.makeText(MainActivityRegistro.this, "Debes ingresar una contraseña", Toast.LENGTH_SHORT).show();
        }else if (password1.length()==0){
            Toast.makeText(MainActivityRegistro.this, "Debes repetir tu contraseña", Toast.LENGTH_SHORT).show();
        }else if (!password.equals(password1)){
            Toast.makeText(MainActivityRegistro.this, "Las contraseñas deben coincidir", Toast.LENGTH_SHORT).show();
        }



            try {
                if (usuario.length() != 0 && password.length() != 0) {
                    Cursor fila = db.rawQuery("Select usuario, password,nombre from usuarios where usuario= '" + usuario + "' and password='" + password + "'", null);
                    if (fila.moveToFirst()) {
                        String user = fila.getString(0);
                        String pass2 = fila.getString(1);
                        db.close();


                        if (usuario.equals(user) && password.equals(pass2)) {

                            Toast.makeText(this, "Ya estas registrado", Toast.LENGTH_SHORT).show();

                            nom.setText("");
                            usu.setText("");
                            pass.setText("");
                            pass1.setText("");
                        }
                    } else {
                        if(nombre_usu.length()!=0 && usuario.length()!=0 && password.length()!=0 && password1.length()!=0 && password.equals(password1)){

                            ContentValues registro=new ContentValues();
                            registro.put("usuario",usuario);
                            registro.put("password",password);
                            registro.put("nombre",nombre_usu);
                            db.insert("usuarios",null,registro);
                            db.close();
                            nom.setText("");
                            usu.setText("");
                            pass.setText("");
                            pass1.setText("");

                            Toast.makeText(MainActivityRegistro.this, "!Cuenta creada con exito¡", Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(MainActivityRegistro.this,MainActivity2.class);
                            startActivity(i);
                        }
                    }
                }
            }catch (Exception e){
                Toast toast=Toast.makeText(this,"Error" + e.getMessage(),Toast.LENGTH_LONG);
                toast.show();
            }



    }
}