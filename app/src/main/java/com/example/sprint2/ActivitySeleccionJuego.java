package com.example.sprint2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ActivitySeleccionJuego extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccion_juego);
    }


    public void JuegoWarzone (View view) {
        Intent siguiente = new Intent(this, MainActivity.class);
        startActivity(siguiente);
    }

    public void JuegoElden (View view) {
        Intent siguiente = new Intent(this, ActivityJuegoElden.class);
        startActivity(siguiente);
    }
}