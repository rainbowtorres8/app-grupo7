package com.example.sprint2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.RemoteInput;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    private EditText nu;
    private EditText ps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        nu=(EditText)findViewById(R.id.txt_Nombre);
        ps=(EditText)findViewById(R.id.txt_password);
    }

    //metodo para el Boton Ingresar
    public void iniciar(View view) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();

        String nombre = nu.getText().toString();
        String password = ps.getText().toString();

        if (nombre.length() == 0) {
            Toast.makeText(MainActivity2.this, "Debes ingresar el nombre de Usuario", Toast.LENGTH_LONG).show();
        }

        if (password.length() == 0) {
            Toast.makeText(MainActivity2.this, "Debes ingresar una Contraseña", Toast.LENGTH_LONG).show();
        }

        try {
            if (nombre.length() != 0 && password.length() != 0) {
                Cursor fila = db.rawQuery("Select usuario, password,nombre from usuarios where usuario= '" + nombre + "' and password='" + password + "'", null);
                if (fila.moveToFirst()) {
                    String user = fila.getString(0);
                    String pass = fila.getString(1);
                    db.close();

                    if (nombre.equals(user) && password.equals(pass)) {
                        nu.setText("");
                        ps.setText("");
                        Intent i = new Intent(MainActivity2.this, ActivitySeleccionJuego.class);
                        startActivity(i);
                    }
                } else {
                    Toast toast=Toast.makeText(this,"Datos incorrectos",Toast.LENGTH_LONG);
                    toast.show();
                    nu.setText("");
                    ps.setText("");
                }
            }
        }catch (Exception e){
           Toast toast=Toast.makeText(this,"Error" + e.getMessage(),Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void crear(View view){
        Intent crear = new Intent(this, MainActivityRegistro.class);
        startActivity(crear);
    }
}